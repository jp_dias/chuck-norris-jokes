//
//  Category.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 29/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

public struct JokeCategory: Decodable, Equatable {
    public let name: String

    init(name: String) {
        self.name = name
    }
}

//
//  JokeQuery.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 29/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

struct JokeQuery: Decodable {
    let total: Int
    let result: [Joke]
}

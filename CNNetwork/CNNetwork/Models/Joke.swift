//
//  Joke.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 29/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

public struct Joke: Decodable, Equatable {
    public let categories: [JokeCategory]?
    public let id: String
    public let value: String
}

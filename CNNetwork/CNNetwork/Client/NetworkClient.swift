//
//  NetworkClient.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 29/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import Alamofire
import RxSwift

final class NetworkClient {
    let manager: SessionManager
    let queue = DispatchQueue(label: "network-client")
    let baseURL = "https://api.chucknorris.io"

    init() {
        manager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
    }
}

extension NetworkClient: RequestProtocol {
    func request<Response>(_ endpoint: Endpoint<Response>) -> Observable<Response> {
        return Observable.create { observer -> Disposable in
            self.manager.request(self.baseURL + endpoint.path,
                                 method: endpoint.method.http,
                                 parameters: endpoint.parameters)
                .validate()
                .responseData(queue: self.queue) { response in
                    let result = response.result.flatMap(endpoint.decode)
                    DispatchQueue.main.async {
                        switch result {
                        case let .success(value):
                            observer.onNext(value)
                        case let .failure(error):
                            observer.onError(error)
                        }
                    }
                }
            return Disposables.create()
        }
    }

    func load<Response>(_ endpoint: Endpoint<Response>) -> Observable<Response> {
        return Observable.create { observer -> Disposable in
            if let path = Bundle.main.path(forResource: endpoint.identifier.rawValue, ofType: "json") {
                DispatchQueue.main.async {
                    do {
                        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                        let result = try endpoint.decode(data)
                        observer.onNext(result)
                    } catch {
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
}

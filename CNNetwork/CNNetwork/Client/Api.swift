//
//  Api.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 29/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import RxSwift

/// Used to indentify the name of file for the mock version of the request
enum RequestIdentifier: String {
    case randomJoke
    case jokeFromCategory
    case searchQuery
    case jokeCategories
}

public struct Api: ClientProtocol, ServiceProtocol {
    internal let api: RequestProtocol
    public let environment: Stage

    public init(environment: Stage = .production) {
        self.api = NetworkClient()
        self.environment = environment
    }

    public func fetchRandomJoke() -> Observable<Joke> {
        return request(Endpoint(identifier: .randomJoke, path: "/jokes/random"))
    }

    public func fetchRandomJoke(from category: JokeCategory) -> Observable<Joke> {
        return request(Endpoint(identifier: .jokeFromCategory, path: "/jokes/random?category=\(category.name)"))
    }

    public func fetchJokes(containing query: String) -> Observable<[Joke]> {
        return request(Endpoint(identifier: .searchQuery, path: "/jokes/search?query=\(query)", decode: {
            return (try? JSONDecoder().decode(JokeQuery.self, from: $0).result) ?? []
        }))
    }

    public func fetchCategories() -> Observable<[JokeCategory]> {
        return request(Endpoint(identifier: .jokeCategories, path: "/jokes/categories", decode: {
            var list = [JokeCategory]()
            let categories = (try? JSONDecoder().decode([String].self, from: $0)) ?? []
            categories.forEach { list.append(JokeCategory(name: $0)) }
            return list
        }))
    }
}

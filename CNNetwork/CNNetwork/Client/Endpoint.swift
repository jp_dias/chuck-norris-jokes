//
//  Endpoint.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 29/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import Alamofire

typealias Parameters = [String: Any]
typealias Path = String

enum Method {
    case get
}

extension Method {
    var http: HTTPMethod {
        switch self {
        case .get:
            return .get
        }
    }
}

final class Endpoint<Response> {
    let identifier: RequestIdentifier
    let method: Method
    let path: Path
    let parameters: Parameters?
    let decode: (Data) throws -> Response

    init(identifier: RequestIdentifier,
        method: Method = .get,
        path: Path,
        parameters: Parameters? = nil,
        decode: @escaping (Data) throws -> Response) {
        self.identifier = identifier
        self.method = method
        self.path = path
        self.parameters = parameters
        self.decode = decode
    }
}

extension Endpoint where Response: Decodable {
    convenience init(identifier: RequestIdentifier, method: Method = .get, path: Path, parameters: Parameters? = nil) {
        self.init(identifier: identifier, method: method, path: path, parameters: parameters) {
            try JSONDecoder().decode(Response.self, from: $0)
        }
    }
}

extension Endpoint where Response == Void {
    convenience init(identifier: RequestIdentifier, method: Method = .get, path: Path, parameters: Parameters? = nil) {
        self.init(identifier: identifier, method: method, path: path, parameters: parameters, decode: { _ in () })
    }
}

//
//  ClientProtocol.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Pedro Dias. All rights reserved.
//

import RxSwift

public enum Stage {
    case testing
    case production
}

protocol ClientProtocol {
    var api: RequestProtocol { get }
    var environment: Stage { get }

    func request<Response>(_ endpoint: Endpoint<Response>) -> Observable<Response>
}

extension ClientProtocol {
    func request<Response>(_ endpoint: Endpoint<Response>) -> Observable<Response> {
        return environment == .production ? api.request(endpoint) : api.load(endpoint)
    }
}

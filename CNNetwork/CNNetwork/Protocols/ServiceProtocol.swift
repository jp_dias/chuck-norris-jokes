//
//  ServiceProtocol.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Pedro Dias. All rights reserved.
//

import RxSwift

public protocol ServiceProtocol {
    /// Fetch random joke.
    func fetchRandomJoke() -> Observable<Joke>
    /// Fetch random joke from a given category.
    func fetchRandomJoke(from category: JokeCategory) -> Observable<Joke>
    /// Fetch list of jokes containing the given query.
    func fetchJokes(containing query: String) -> Observable<[Joke]>
    /// Fetch available categories.
    func fetchCategories() -> Observable<[JokeCategory]>
}

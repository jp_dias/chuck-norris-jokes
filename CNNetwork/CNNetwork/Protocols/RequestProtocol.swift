//
//  RequestProtocol.swift
//  CNNetwork
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Pedro Dias. All rights reserved.
//

import RxSwift

protocol RequestProtocol {
    /// Make request to the webservice.
    func request<Response>(_ endpoint: Endpoint<Response>) -> Observable<Response>
    /// Make request to a mocked response.
    func load<Response>(_ endpoint: Endpoint<Response>) -> Observable<Response>
}

//
//  JokeDetailsView.swift
//  CNJokes
//
//  Created by João Pedro Dias on 07/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import UIKit

final class JokeDetailsView: UIView {
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Styles.Colors.white
        return scrollView
    }()

    private let contentView = UIView()

    let jokeContentLabel: UILabel = {
        let label = UILabel()
        label.font = Styles.Fonts.body
        label.textColor = Styles.Colors.black
        label.textAlignment = .natural
        label.numberOfLines = 0
        return label
    }()

    let randomJokeButton: UIButton = {
        let button = UIButton()
        button.setTitle("joke_details_get_new_joke".localized, for: .normal)
        button.backgroundColor = Styles.Colors.gray
        button.contentEdgeInsets = Styles.Margins.regularButtonContentInsets
        button.layer.cornerRadius = Styles.Dimensions.defaultCornerRadius
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup() {
        setupScrollView()
        setupContentView()
        setupJokeContentLabel()
        setupRandomJokeButton()
    }

    private func setupScrollView() {
        addSubview(scrollView)
        scrollView.fillSuperview()
    }

    private func setupContentView() {
        scrollView.addSubview(contentView)
        contentView.fillSuperview()
        contentView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
    }

    private func setupJokeContentLabel() {
        contentView.addSubview(jokeContentLabel)
        jokeContentLabel.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                                leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                                bottom: nil,
                                trailing: contentView.safeAreaLayoutGuide.trailingAnchor,
                                padding: .init(top: 20, left: 20, bottom: 0, right: 20))
    }

    private func setupRandomJokeButton() {
        contentView.addSubview(randomJokeButton)
        randomJokeButton.anchor(top: jokeContentLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,
                             padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        randomJokeButton.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor).isActive = true
        randomJokeButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    }
}

//
//  JokeDetailsViewController.swift
//  CNJokes
//
//  Created by João Pedro Dias on 07/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import CNNetwork
import RxCocoa
import RxSwift
import UIKit

final class JokeDetailsViewController: UIViewController, HasCustomView {
    typealias CustomView = JokeDetailsView
    private let disposeBag = DisposeBag()

    private let category: JokeCategory

    init(category: JokeCategory) {
        self.category = category
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = JokeDetailsView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bindViewModel()
    }

    private func setupUI() {
        title = category.name.capitalized
    }

    private func bindViewModel() {
        let (
            jokeContentLabelText,
            _
        ) = jokeDetailsViewModel(
            category: category,
            randomJokeButtonTap: customView.randomJokeButton.rx.tap.asObservable(),
            viewDidLoad: .just(())
        )

        customView.randomJokeButton.rx.tap
            .bind {
                self.customView.randomJokeButton.isEnabled = false
                self.customView.jokeContentLabel.text = "joke_details_loading_joke_content".localized
            }
            .disposed(by: disposeBag)

        jokeContentLabelText
            .bind { self.customView.jokeContentLabel.text = $0 }
            .disposed(by: disposeBag)

        jokeContentLabelText
            .bind { _ in self.customView.randomJokeButton.isEnabled = true }
            .disposed(by: disposeBag)
    }
}

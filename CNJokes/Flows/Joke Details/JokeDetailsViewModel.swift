//
//  JokeDetailsViewModel.swift
//  CNJokes
//
//  Created by João Pedro Dias on 07/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNNetwork
import RxSwift
import UIKit

func jokeDetailsViewModel
(
    category: JokeCategory,
    randomJokeButtonTap: Observable<Void>,
    viewDidLoad: Observable<Void>
) -> (
    jokeContentLabelText: Observable<String>,
    nothing: Observable<Void>
) {
    let fetchRequest = Observable.merge(
        randomJokeButtonTap,
        viewDidLoad
    )

    let joke = fetchRequest
        .flatMapLatest { return Api().fetchRandomJoke(from: category).retry(3) }
        .share()

    let jokeContentLabelText = joke
        .map { $0.value }
        .startWith("joke_details_loading_joke_content".localized)

    return (
        jokeContentLabelText: jokeContentLabelText,
        nothing: .never()
    )
}

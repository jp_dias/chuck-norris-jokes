//
//  SearchJokesViewModel.swift
//  CNJokes
//
//  Created by João Pedro Dias on 08/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNNetwork
import RxCocoa
import RxSwift
import UIKit

func searchJokesViewModel(
    searchInput: Observable<String?>
) -> (
    jokesContentList: Driver<[String]>,
    nothing: Observable<Void>
) {
    let jokes = searchInput
        .flatMap { Observable.from(optional: $0) }
        .filter { $0.count > 2 }
        .throttle(1.0, scheduler: MainScheduler.instance)
        .flatMapLatest { Api().fetchJokes(containing: $0).retry(3) }
        .share()

    let jokesContentList = jokes
        .map { jokes in jokes.compactMap { $0.value } }
        .asDriver(onErrorJustReturn: [])

    return (
        jokesContentList: jokesContentList,
        nothing: .never()
    )
}

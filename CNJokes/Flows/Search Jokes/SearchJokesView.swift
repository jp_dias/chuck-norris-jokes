//
//  SearchJokesView.swift
//  CNJokes
//
//  Created by João Pedro Dias on 08/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import UIKit

final class SearchJokesView: UIView {
    let jokesTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        tableView.allowsSelection = false
        tableView.isScrollEnabled = true
        tableView.backgroundColor = Styles.Colors.white
        tableView.separatorColor = Styles.Colors.black
        tableView.separatorInset = .zero
        tableView.tableFooterView = UIView()
        return tableView
    }()

    let noJokesFoundView: UIView = {
        let view = UIView()
        view.backgroundColor = Styles.Colors.gray
        view.isHidden = false
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup() {
        setupJokeTableView()
        setupNoJokesFoundView()
    }

    private func setupJokeTableView() {
        addSubview(jokesTableView)
        jokesTableView.fillSuperview()
    }

    private func setupNoJokesFoundView() {
        addSubview(noJokesFoundView)
        noJokesFoundView.fillSuperview()
    }
}

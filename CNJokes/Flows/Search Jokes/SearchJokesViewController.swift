//
//  SearchJokesViewController.swift
//  CNJokes
//
//  Created by João Pedro Dias on 08/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import RxCocoa
import RxSwift
import UIKit

final class SearchJokesViewController: UIViewController, HasCustomView {
    typealias CustomView = SearchJokesView
    private let disposeBag = DisposeBag()

    private let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.placeholder = "joke_search_search_bar_placeholder".localized
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        return searchController
    }()

    override func loadView() {
        view = SearchJokesView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bindViewModel()
    }

    private func setupUI() {
        title = "search_jokes_view_title".localized
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }

    private func bindViewModel() {
        let (jokesContentList, _) = searchJokesViewModel(
            searchInput: searchController.searchBar.rx.text.asObservable()
        )

        jokesContentList
            .drive(customView.jokesTableView.rx.items(cellIdentifier: "cellIdentifier")) { _, element, cell in
                cell.textLabel?.text = element
                cell.textLabel?.numberOfLines = 0
            }
            .disposed(by: disposeBag)

        jokesContentList
            .drive(onNext: { [weak self] jokes in
                self?.customView.noJokesFoundView.isHidden = jokes.count != 0
            })
            .disposed(by: disposeBag)

    }
}

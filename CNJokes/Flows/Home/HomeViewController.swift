//
//  HomeViewController.swift
//  CNJokes
//
//  Created by João Pedro Dias on 31/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import CNNetwork
import RxCocoa
import RxSwift
import UIKit

final class HomeViewController: UIViewController, HasCustomView {
    typealias CustomView = HomeView

    private let disposeBag = DisposeBag()

    private let searchBarItem = UIBarButtonItem(barButtonSystemItem: .search, target: nil, action: nil)

    override func loadView() {
        view = HomeView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bindViewModel()
        bindTransition()
    }

    private func setupUI() {
        title = "home_view_title".localized
        navigationItem.rightBarButtonItem = searchBarItem
    }

    private func bindViewModel() {
        guard let refreshControl = customView.categoryCollectionView.refreshControl else {
            fatalError("Missing Refresh Control on Category Collection.")
        }
        let (
            categories,
            _
        ) = homeViewModel(
            refreshControl: refreshControl.rx.controlEvent(.valueChanged).asObservable(),
            viewDidLoad: .just(())
        )

        categories
            .drive(customView.categoryCollectionView.rx.items(cellIdentifier: CategorySquareCell.identifier,
                                                              cellType: CategorySquareCell.self)
            ) { [weak self] _, element, cell in
                cell.item = element
                self?.bindTransition(to: cell)

                if refreshControl.isRefreshing {
                    refreshControl.endRefreshing()
                }
            }
            .disposed(by: disposeBag)
    }

    private func bindTransition() {
        searchBarItem.rx.tap
            .bind {
                self.navigationController?.pushViewController(SearchJokesViewController(), animated: true)
            }
            .disposed(by: disposeBag)
    }

    private func bindTransition(to cell: CategorySquareCell) {
        let tapGesture = UITapGestureRecognizer()
        cell.addGestureRecognizer(tapGesture)
        tapGesture.rx.event
            .bind(onNext: { [weak self] _ in
                guard let category = cell.item else {
                    fatalError("Missing category to present joke.")
                }
                let controller = JokeDetailsViewController(category: category)
                self?.navigationController?.pushViewController(controller, animated: true)
            })
            .disposed(by: disposeBag)
    }
}

//
//  HomeViewModel.swift
//  CNJokes
//
//  Created by João Pedro Dias on 31/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNNetwork
import RxCocoa
import RxSwift
import UIKit

func homeViewModel(
    refreshControl: Observable<Void>,
    viewDidLoad: Observable<Void>
) -> (
    categories: Driver<[JokeCategory]>,
    nothing: Observable<Void>
) {
    let fetchRequest = Observable.merge(
        refreshControl,
        viewDidLoad
    )

    let categories = fetchRequest
        .flatMapLatest { return Api().fetchCategories().retry(3) }
        .share()
        .asDriver(onErrorJustReturn: [])

    return (
        categories: categories,
        nothing: .never()
    )
}

// 
//  HomeView.swift
//  CNJokes
//
//  Created by João Pedro Dias on 31/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import UIKit

final class HomeView: UIView {

    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()

    let categoryCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.estimatedItemSize = Styles.Dimensions.categorySquareBox

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.register(CategorySquareCell.self, forCellWithReuseIdentifier: CategorySquareCell.identifier)
        collectionView.contentInset = Styles.Margins.squareCollectionInsets
        collectionView.backgroundColor = Styles.Colors.white

        let refreshControl = UIRefreshControl()
        collectionView.refreshControl = refreshControl
        return collectionView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup() {
        setupStackView()
        setupCategoryCollectionView()
    }

    private func setupStackView() {
        addSubview(stackView)
        stackView.fillSuperview()
    }

    private func setupCategoryCollectionView() {
        stackView.addArrangedSubview(categoryCollectionView)
    }
}

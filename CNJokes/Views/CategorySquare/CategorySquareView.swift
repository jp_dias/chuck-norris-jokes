//
//  CategorySquareView.swift
//  CNJokes
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import CNLibrary
import UIKit

public final class CategorySquareView: UIView {
    let squareView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = Styles.Colors.darkGray
        view.layer.cornerRadius = Styles.Dimensions.defaultCornerRadius
        return view
    }()

    let centeredView = UIView()

    let iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = Styles.Colors.white
        return imageView
    }()

    let categoryNameLabel: UILabel = {
        let label = UILabel()
        label.font = Styles.Fonts.highlight
        label.textColor = Styles.Colors.white
        label.textAlignment = .center
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup() {
        setupSquareView()
        setupCenteredView()
        setupIconView()
        setupCategoryNameLabel()
    }

    private func setupSquareView() {
        addSubview(squareView)
        squareView.fillSuperview()
        squareView.widthAnchor.constraint(equalTo: squareView.heightAnchor).isActive = true
    }

    private func setupCenteredView() {
        squareView.addSubview(centeredView)
        centeredView.center(to: squareView)
    }

    private func setupIconView() {
        centeredView.addSubview(iconView)
        iconView.anchor(top: centeredView.topAnchor, leading: centeredView.leadingAnchor,
                        bottom: nil, trailing: centeredView.trailingAnchor)
    }

    private func setupCategoryNameLabel() {
        centeredView.addSubview(categoryNameLabel)
        categoryNameLabel.anchor(top: iconView.bottomAnchor, leading: centeredView.leadingAnchor,
                                 bottom: centeredView.bottomAnchor, trailing: centeredView.trailingAnchor,
                                 padding: .init(top: 30, left: 0, bottom: 0, right: 0))
    }
}

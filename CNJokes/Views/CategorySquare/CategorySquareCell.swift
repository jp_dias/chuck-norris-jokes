//
//  CategorySquareCell.swift
//  CNJokes
//
//  Created by João Pedro Dias on 31/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import UIKit
import CNNetwork
import CNLibrary

final class CategorySquareCell: UICollectionViewCell {
    static let identifier = "categorySquareCell"

    var item: JokeCategory? {
        didSet {
            squareView.categoryNameLabel.text = item?.name
            squareView.iconView.image = item?.icon.withRenderingMode(.alwaysTemplate)
        }
    }

    let squareView = CategorySquareView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup() {
        setupSquareView()
    }

    private func setupSquareView() {
        contentView.addSubview(squareView)
        squareView.fillSuperview()
    }
}

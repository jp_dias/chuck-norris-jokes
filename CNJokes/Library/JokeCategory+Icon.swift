//
//  JokeCategory+Icon.swift
//  CNJokes
//
//  Created by João Pedro Dias on 31/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import UIKit
import CNNetwork

extension JokeCategory {
    var icon: UIImage {
        return UIImage(named: "category-" + name) ?? #imageLiteral(resourceName: "category-other")
    }
}

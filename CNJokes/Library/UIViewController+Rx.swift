//
//  UIViewController+Rx.swift
//  CNJokes
//
//  Created by João Pedro Dias on 04/02/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import RxSwift
import RxCocoa

public extension Reactive where Base: UIViewController {
    public var viewDidLoad: ControlEvent<Void> {
        let source = methodInvoked(#selector(Base.viewDidLoad)).map { _ in }
        return ControlEvent(events: source)
    }

    public var viewWillAppear: ControlEvent<Void> {
        let source = methodInvoked(#selector(Base.viewWillAppear)).map { _ in }
        return ControlEvent(events: source)
    }
}

//
//  Colors.swift
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import UIKit

public extension Styles {
    public struct Colors {
        public static let white = UIColor.white
        public static let darkGray = UIColor.darkGray
        public static let gray = UIColor.gray
        public static let black = UIColor.black
    }
}

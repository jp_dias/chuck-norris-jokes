//
//  Fonts.swift
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import UIKit

public extension Styles {
    public struct Fonts {
        public static let body = UIFont.preferredFont(forTextStyle: .body)
        public static let highlight = UIFont.systemFont(ofSize: 16)
    }
}

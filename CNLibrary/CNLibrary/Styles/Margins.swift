//
//  Margins.swift
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

public extension Styles {
    public struct Margins {
        public static let squareCollectionInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        public static let regularButtonContentInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}

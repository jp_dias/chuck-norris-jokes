//
//  Dimensions.swift
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

public extension Styles {
    public struct Dimensions {
        public static let categorySquareBox = CGSize(width: 180, height: 180)
        public static let defaultCornerRadius: CGFloat = 4.0
    }
}

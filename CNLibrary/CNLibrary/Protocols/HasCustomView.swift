//
//  HasCustomView.swift
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import UIKit

public protocol HasCustomView {
    associatedtype CustomView: UIView
}

public extension HasCustomView where Self: UIViewController {
    public var customView: CustomView {
        guard let customView = view as? CustomView else {
            fatalError("Expected view to be of type \(CustomView.self) but got \(type(of: view)) instead.")
        }
        return customView
    }
}

//
//  UIView+Anchors.swift
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Dias. All rights reserved.
//

import UIKit

public extension UIView {
    public func fillSuperview(padding: UIEdgeInsets) {
        anchor(top: superview?.topAnchor,
               leading: superview?.leadingAnchor,
               bottom: superview?.bottomAnchor,
               trailing: superview?.trailingAnchor,
               padding: padding)
    }

    public func fillSuperview() {
        fillSuperview(padding: .zero)
    }

    public func anchorSize(to view: UIView) {
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }

    public func center(to view: UIView, shouldConstrain: Bool = true) {
        self.translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        if shouldConstrain {
            widthAnchor.constraint(lessThanOrEqualTo: view.widthAnchor).isActive = true
            heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor).isActive = true
        }
    }

    public func anchor(top: NSLayoutYAxisAnchor?,
                       leading: NSLayoutXAxisAnchor?,
                       bottom: NSLayoutYAxisAnchor?,
                       trailing: NSLayoutXAxisAnchor?,
                       padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}

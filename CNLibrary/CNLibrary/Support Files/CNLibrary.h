//
//  CNLibrary.h
//  CNLibrary
//
//  Created by João Pedro Dias on 30/01/2019.
//  Copyright © 2019 João Pedro Dias. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CNLibrary.
FOUNDATION_EXPORT double CNLibraryVersionNumber;

//! Project version string for CNLibrary.
FOUNDATION_EXPORT const unsigned char CNLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CNLibrary/PublicHeader.h>



[TOC]

# Chuck Norris Jokes
------

This project is based on [Chuck Norris API](https://api.chucknorris.io), providing a list of jokes about the mythical "Chuck Norris".

It's a test project that aims to be a plataform for developers to learn, implement and test any iOS pattern.

Feel free to fork and make pull requests.

------
## To-do

* Test coverage.
* Implement (Flow) Coordinator pattern.
...


------
## Install guide

1. Clone the repo
2. Open `terminal` and do `pod install`
3. Open `.xcworkspace` file
4. Start coding!

